class BuildingManager {
    constructor() {
        this.grid = new Array();
        this.effectGrid = new Array();
        this.blockEffect = false;
        this.laboratories = [];
        this.electricities = [];
        this.buildingMenuSelected = null;
        this.rows = 5;
        this.cols = 5;
        this.autoRebuild = false;
    }
   
    init() {
        this.blockEffect = true;
        
        this.constructBuildMenu();
        this.rows = store.grid.rows;
        this.cols = store.grid.cols;
        
        this.createGrid();
        this.populateGrid();

        this.blockEffect = false;
        this.applyAllEffectsFromGrid();
    }
    
    actualiseGridEffectIcons(row, col) {
        const id = this.createId(row, col);
        for (const effectName in store.effects) {
            if (undefined === store.effects[effectName].img) {
                continue;
            }
            const img = refManager.get("dom", "icon-effect-"+effectName+"-"+id);
            if (this.effectGrid[row][col].includes(effectName)) {
                img.classList.remove("invisible");
            } else {
                img.classList.add("invisible");
            }
        }

        const img = refManager.get("dom", "icon-effect-dune-"+id);
        if ("dune" === this.grid[row][col]?.name && -1 === this.grid[row][col]?.modif) {
            img.classList.remove("invisible");
        } else {
            img.classList.add("invisible");
        }
    }

    actualiseGridTooltip(building, id) {
        if (null === building)
            building = this.getEmptySpace();

        // ex : tt-grid-14-name
        const getRef = (ct) => refManager.get("dom", `tt-grid-${id}-${ct}`);
        const actRef = (ct, content, show) => {
            const ref = getRef(ct);
            ref.innerHTML = `${content}`;
            if (show) {
                ref?.parentNode?.classList.remove('invisible');
            } else {
                ref?.parentNode?.classList.add('invisible');
            }
        }
        const [row, col] = this.getRowColFromId(id);

        actRef("name", building.realName, true);
        actRef("description", building.description, true);

        actRef("effect", 
            Object.entries(building.effect).map(([effectName, _]) => `${effectName} (${undefined === building.effect[effectName].xNeighbors ? "global" : `${building.effect[effectName].xNeighbors}-n`})`).join(" | "), 
            Object.keys(building.effect).length > 0);

        actRef("produce", 
            Object.entries(building.produce).map(([resName, resProd]) => `${resName} ${formatNumber(resProd, 2)}`).join(" | "), 
            Object.keys(building.produce).length > 0);

        actRef("use", 
            Object.entries(building.usage).map(([resName, resUse]) => `${resName} ${resUse}`).join(" | "), 
            Object.keys(building.usage).length > 0);

        actRef("under-effect", 
            this.effectGrid[row][col].join(" | "), 
            this.effectGrid[row][col].length > 0);

        actRef("durability", building.maxLife, building.maxLife > 0);
    }

    addEffect(row, col, effName, deleteEffect) {
        if (deleteEffect && !!!this.effectGrid[row][col].includes(effName))
            return;
        if (!!!deleteEffect && this.effectGrid[row][col].includes(effName))
            return;

        if (deleteEffect) {
            delElem(this.effectGrid[row][col], effName);
        } else {
            this.effectGrid[row][col].push(effName);
        }

        if (!!!this.blockEffect)
            this.applyEffect(this.grid[row][col], effName, deleteEffect);
    }
    
    advanceProd(building, row, col, id, filter) {
        if (null === building) {
            return;
        }

        if ("laboratory" === building.name) {
            return;
        }
        
        const producingElectricity = Object.keys(this.grid[row][col].produce).includes("electricity");
        if ((producingElectricity && filter !== "electricity") || (!!!producingElectricity && filter === "electricity")) {
            return;
        }

        const [production, usage, deletion, construction] = this.grid[row][col].advance(deltaTime);
        if (deletion) {
            this.destructBuilding(row, col, this.grid[row][col].name !== "dune")
        }

        if (construction) {
            this.distributeEffect(row, col, this.grid[row][col].effect);
        }

        resourceManager.tickProd(production, usage);
    }

    advanceTime(filter=null) {
        if ("laboratories" === filter) {
            for (const id of deepCopy(this.laboratories)) {
                const [row, col] = this.getRowColFromId(id);
                const [production, usage, deletion, construction] = this.grid[row][col].advance(deltaTime);
                if (deletion) {
                    this.destructBuilding(row, col, this.grid[row][col].name !== "dune");
                }
                if (!!!("research" in production)) {
                    continue;
                }
                const pc = resourceManager.tickProd({}, usage);
                upgradeManager.researchTick(pc*production.research);
            }
            
            return;
        }

        for (let row=0; row<this.rows; row++) {
            for (let col=0; col<this.cols; col++) {
                const building = this.grid[row][col];
                const id = this.createId(row, col);
                this.advanceProd(building, row, col, id, filter);
            }
        }
    }

    applyAllEffectsFromGrid() {
        for (let row=0; row<this.rows; row++) {
            for (let col=0; col<this.cols; col++) {
                this.applyEffectFromGrid(row, col);
            }
        }
    }

    applyEffectFromGrid(row, col) {
        if (this.blockEffect)
            return;
        for (const effName of this.effectGrid[row][col]) {
            this.applyEffect(this.grid[row][col], effName);
        }
        this.actualiseGridEffectIcons(row, col);
        this.actualiseGridTooltip(this.grid[row][col], this.createId(row, col));
    }

    applyEffect(building, effect, deleteEffect=false) {
        if (null === building || undefined === building)
            return;

        if (EFFECT_STOCK_ELECTRICITY === effect) {
            const stock = store.effects[EFFECT_STOCK_ELECTRICITY].quantity * (deleteEffect ? -1 : 1);
            resourceManager.changeStockResource("electricity", stock);
            return;
        }

        if (EFFECT_PROTECTION === effect) {
            let durability = store.effects[EFFECT_PROTECTION].durability;
            if (!!!deleteEffect) {
                durability = 1 / durability;
            }
            building.desSpeed *= durability;
            return;
        }

        if (EFFECT_CONSTRUCTION === effect) {
            let speed = store.effects[EFFECT_CONSTRUCTION].speed;
            if (deleteEffect) {
                speed = 1 / speed;
            }
            building.consSpeed *= speed;
            return;
        }

        if (EFFECT_CONSUMPTION === effect) {
            let consumption = store.effects[EFFECT_CONSUMPTION].consumption;
            if (deleteEffect) {
                consumption = 1 / consumption;
            }
            if (Object.keys(building.usage).includes("electricity")) {
                building.usage["electricity"] *= consumption;
            }
            return;
        }

        if (EFFECT_PRODUCTION === effect) {
            let speed = store.effects[EFFECT_PRODUCTION].speed;
            if (deleteEffect) {
                speed = 1 / speed;
            }
            for (const resName in building.produce) {
                building.produce[resName] *= speed;
            }
            return;
        }

        if (EFFECT_AUTOMATISATION_CONSTRUCTION === effect) {
            this.autoRebuild = !deleteEffect;
            return;
        }
    }

    build(id, name) {
        this.factory(name, id);
        const img = refManager.get("dom", "image-"+id);
        const progress = refManager.get("dom", "progress-bar-"+id);
        img.classList.remove("invisible");
        progress.classList.remove("invisible");
    }

    buildMenuTooltip(building, buildingName) {
        // ex: tt-build-dune-name
        const id = (ct) => "id='tt-build-" + buildingName + "-" + ct + "'";

        const separator = "<div class='tooltip-content-separator'>--- ---</div>";
        
        let tooltip = "<div class='tooltip-content'>";

        tooltip += "<div class='tooltip-content-name' " + id("name") + ">" + building.realName + "</div>" + separator;

        if ("dune" === buildingName) {
            tooltip += "<div class='tooltip-content-description' " + id("description") + ">" + "Clean up the debris to make space for construction" + "</div>" + separator;            
            tooltip += "<div class='tooltip-content-cost'>Temporary cost: <span " + id("cost") + ">" + Object.entries(building.cost).map(([resName, resCost]) => `${resName} ${resCost}`).join(" | ") + "<span></div>";
            return tooltip;
        }

        tooltip += "<div class='tooltip-content-description' " + id("description") + ">" + building.description + "</div>" + separator;

        tooltip += "<div class='tooltip-content-cost'>Cost: <span " + id("cost") + ">" + Object.entries(building.cost).map(([resName, resCost]) => `${resName} ${resCost}`).join(" | ") + "<span></div>";

        if (Object.keys(building.effect).length > 0) {
            tooltip += "<div class='tooltip-content-effect'>Effect: <span " + id("effect") + ">" + Object.entries(building.effect).map(([effectName, _]) => `(${undefined === building.effect[effectName].xNeighbors ? "global" : `${building.effect[effectName].xNeighbors}-n`})`).join(" | ") + "<span></div>";
        }

        if (Object.keys(building.produce).length > 0) {
            tooltip += "<div class='tooltip-content-produce'>Produces: <span " + id("produce") + ">" + Object.entries(building.produce).map(([resName, resProd]) => `${resName} ${resProd}`).join(" | ") + "<span></div>";
        }

        if (Object.keys(building.usage).length > 0) {
            tooltip += "<div class='tooltip-content-use'>Uses: <span " + id("use") + ">" + Object.entries(building.usage).map(([resName, resUse]) => `${resName} ${resUse}`).join(" | ") + "<span></div>";
        }

        tooltip += separator;

        tooltip += "<div class='tooltip-content-build'>Building time: <span " + id("build-time") + ">" + secToTime(building.maxLife/building.consSpeed) + "</span></div>";
        tooltip += "<div class='tooltip-content-dest'>Compression time: <span " + id("dest-time") + ">" + secToTime(building.maxLife/building.desSpeed) + "</span></div>";

        tooltip += "</div>";
        return tooltip;
    }

    constructBuildMenu() {
        const buildDiv = refManager.get("dom", "buildings");
        buildDiv.innerHTML = "<div class='title-menu'>Buildings</div>";

        for (const name in store.buildings) {
            const elem = document.createElement("div");
            elem.id = "building-menu-elem-"+name;
            elem.className = "building-menu-elem tooltip tooltip-right";
            if (!!!store.buildings[name].visible) {
                elem.className += " invisible";
            }
            elem.innerHTML = "<img src=assets/img/menu-build/"+name+".png></img>" + this.buildMenuTooltip(store.buildings[name], name); 
            elem.addEventListener("click", () => buildingManager.menuBuildingsClick(name));
            buildDiv.append(elem);
        };
    }

    createEmptySpace(id) {
        const img = refManager.get("dom", "image-"+id);
        const progress = refManager.get("dom", "progress-bar-"+id);
        img.classList.add("invisible");
        progress.classList.add("invisible");
        this.actualiseGridTooltip(this.getEmptySpace(), id);
    }

    createGrid() {
        const rows = this.rows;
        const cols = this.cols;
        const pcRows = Math.floor(100/rows);
        const gridRef = refManager.get("dom", "grid");
        gridRef.innerHTML = "";
        document.documentElement.style.setProperty('--grid-cols-val', rows.toString());
        document.documentElement.style.setProperty('--grid-cols-pc', pcRows.toString()+'%');
        
        const generateAllImgEffects = (id) => {
            let img = "";
            for (const effect in store.effects) {
                const imgName = store.effects[effect].img;
                if (undefined === imgName) {
                    continue;
                }
                img += `<img class='invisible' src='assets/img/effects/${imgName}.png' id='icon-effect-${effect}-${id}'>`
            }
            img += `<img class='invisible' src='assets/img/menu-build/dune.png' id='icon-effect-dune-${id}'>`;
            
            return img;
        };

        for (let i=0; i<rows; i++) {
            for (let j=0; j<cols; j++) {
                const id = (i*cols + j).toString();
                const elem = document.createElement("div");
                elem.className = "cell tooltip";
                if (i/rows < 0.5) {
                    elem.className += " tooltip-bottom";
                } else {
                    elem.className += " tooltip-top";
                }
                elem.innerHTML = "<div id='cell-"+id+"'><img id='image-"+id+"'></img><div class='progress-bar' id='progress-bar-"+id+"'><div id='slider-"+id+"'></div></div><div class='grid-effects' id='grid-effects-"+id+"'>"+generateAllImgEffects(id)+"</div></div>" + this.gridTooltip(id);
                elem.addEventListener("click", () => buildingManager.gridClick(id));
                gridRef.appendChild(elem);
            }
        }
    }
    
    createId(row, col) {
        return row*this.cols + col
    }
    
    createRubble(life, resources, id) {
        const ref = this.prepareCopyOfStore("dune");

        ref.maxLife = Math.max(life, store.buildings.dune.maxLife);
        ref.life = ref.maxLife;
        ref.produce = this.distributeResourcesOnDune(resources, ref);
        this.actualiseGridTooltip(ref, id);

        return new Building("dune", id, ref);
    }
    
    destructBuilding(row, col, makeRubble) {
        const resources = this.grid[row][col].endProduce;
        const id = this.createId(row, col);

        const effects = this.grid[row][col].effect;
        const DELETE_EFFECTS = true;
        this.distributeEffect(row, col, effects, DELETE_EFFECTS);

        if ("laboratory" === this.grid[row][col].name) {
            delElem(this.laboratories, id);
        } 
        
        if (Object.keys(this.grid[row][col].produce).includes("electricity")) {
            delElem(this.electricities, id);
        }

        if (makeRubble) {
            this.grid[row][col] = this.createRubble(this.grid[row][col].maxLife, resources, id);
        } else {
            resourceManager.tickProd(resources, {});
            this.grid[row][col] = null;
            this.createEmptySpace(id);
        }
        this.actualiseGridEffectIcons(row, col);
    }

    display() {
        for (let row=0; row<this.rows; row++) {
            for (let col=0; col<this.cols; col++) {
                if (null === this.grid[row][col]) {
                    continue;
                }
                this.grid[row][col]?.display();
            }
        }
    }

    distributeEffect(row, col, effect, deleteEffect=false) {
        for (const effName in effect) {
            // global effects here
            if (EFFECT_STOCK_ELECTRICITY === effName) {
                this.applyEffect(this.grid[row][col], effName, deleteEffect);
                continue;
            }
            // local effects here
            const xNeighbors = effect[effName].xNeighbors;
            const neighbors = this.getAllCoorNeighbours(row, col, xNeighbors);
            for (const [rn, cn] of neighbors) {
                this.addEffect(rn, cn, effName, deleteEffect);
                this.actualiseGridTooltip(this.grid[rn][cn], this.createId(rn, cn));
                this.actualiseGridEffectIcons(rn, cn);
            }
        }
    }

    distributeResourcesOnDune(resources, dune) {
        const ps = dune.desSpeed / dune.maxLife;
        const produce = deepCopy(resources);
        for (const res in produce) {
            produce[res] *= ps;
        }
        return produce;
    }
    
    factory(elem, id, jsonBuilding=null) {
        let ref = null;
        const [row, col] = this.getRowColFromId(id);

        if (null !== jsonBuilding) {
            ref = jsonBuilding;
            id = jsonBuilding.id;
            elem = jsonBuilding.name;
        }

        if (null === jsonBuilding) {
            if (
                null === store.buildings[elem] 
                || undefined === store.buildings[elem]
            ) {
                this.createEmptySpace(id);
                this.grid[row][col] = null;
                return;
            }

            ref = this.prepareCopyOfStore(elem);
        }

        this.actualiseGridTooltip(ref, id);
        if ("laboratory" === elem) {
            this.laboratories.push(id);
        }
        if (Object.keys(ref.produce).includes("electricity")) {
            this.electricities.push(id);
        }

        const building = new Building(elem, id, ref);
        this.grid[row][col] = building;
        if (null !== jsonBuilding && jsonBuilding.modif < 0) {
            this.distributeEffect(row, col, jsonBuilding.effect);
        }
        this.applyEffectFromGrid(row, col);
    }

    getAllCoorNeighbours(row, col, xNeighbors=4) {
        let arr = [];
        let filter;
        switch (xNeighbors) {
            case 4:
                filter = [
                    [0,0,0,0,0],
                    [0,0,1,0,0],
                    [0,1,0,1,0],
                    [0,0,1,0,0],
                    [0,0,0,0,0],
                ]
                break;
            case 8:
                filter = [
                    [0,0,0,0,0],
                    [0,1,1,1,0],
                    [0,1,0,1,0],
                    [0,1,1,1,0],
                    [0,0,0,0,0],
                ]
                break;
            case 12:
                filter = [
                    [0,0,1,0,0],
                    [0,1,1,1,0],
                    [1,1,0,1,1],
                    [0,1,1,1,0],
                    [0,0,1,0,0],
                ]
                break;
            default:
                console.error("xNeighbor not supported", xNeighbors);
                filter = [
                    [0,0,0,0,0],
                    [0,0,0,0,0],
                    [0,0,0,0,0],
                    [0,0,0,0,0],
                    [0,0,0,0,0],
                ]
                break;
        }

        for (let r=0; r<5; r++) {
            for (let c=0; c<5; c++) {
                if (filter[r][c] === 1) {
                    arr.push([row+r-2, col+c-2]);
                }
            }
        }
        
        return arr.filter(rc => !(rc[0] < 0 || rc[1] < 0 || rc[0] >= this.rows || rc[1] >= this.cols));
    }

    getBuilding(id) {
        const col = id%this.cols;
        const row = Math.floor(id/this.cols);
        return this.grid[row][col];
    }

    getEmptySpace() {
        return {name: 'empty', maxLife: 0, produce: {}, effect: {}, usage: {}, description: "just an empty space, perfect to build", realName: "Empty space"};
    }

    getMaxLabConsumption() {
        let consumption = 0;
        for (const id of this.laboratories) {
            const [row, col] = this.getRowColFromId(id);
            consumption += this.grid[row][col].produce.research;
        }
        return consumption;
    }

    getRowColFromId(id) {
        return [Math.floor(id/this.cols), id%this.cols];
    }

    gridClick(id) {
        if (null !== this.buildingMenuSelected) {
            this.tryToBuild(id);
        }
    }

    gridTooltip(id) {
        // ex : tt-grid-14-name
        const ident = (ct) => "id='tt-grid-" + id + "-" + ct + "'";

        const separator = "<div class='tooltip-content-separator'>--- ---</div>";
        
        let tooltip = "<div class='tooltip-content'>";

        tooltip += "<div class='tooltip-content-name' " + ident("name") + "></div>" + separator;

        tooltip += "<div class='tooltip-content-description' " + ident("description") + "></div>" + separator;

        tooltip += "<div class='tooltip-content-effect invisible'>Effect: <span " + ident("effect") + "><span></div>";

        tooltip += "<div class='tooltip-content-produce invisible'>Produces: <span " + ident("produce") + "><span></div>";

        tooltip += "<div class='tooltip-content-use invisible'>Uses: <span " + ident("use") + "><span></div>";

        tooltip += separator;

        tooltip += "<div class='tooltip-content-under-effect invisible'>Under effects: <span " + ident("under-effect") + "><span></div>";

        tooltip += "<div class='tooltip-content-durability invisible'>Resistance: <span " + ident("durability") + "><span></div>";

        tooltip += "</div>";
        return tooltip;
    }

    menuBuildingsClick(name) {  
        const elem = refManager.get("dom", "building-menu-elem-"+name);
        if (name === this.buildingMenuSelected) {
            elem.classList.remove("menu-selected");
            this.buildingMenuSelected = null;
        } else {
            elem.classList.add("menu-selected");
            if (null !== this.buildingMenuSelected) {
                const elemToReset = refManager.get("dom", "building-menu-elem-"+this.buildingMenuSelected);
                elemToReset.classList.remove("menu-selected");
            }
            this.buildingMenuSelected = name;
        }
    }

    populateGrid() {
        for (let row=0; row<this.rows; row++) {
            this.effectGrid[row] = new Array(this.cols);
            for (let col=0; col<this.cols; col++) {
                this.effectGrid[row][col] = new Array();
            }
        }

        if (store.grid.grid.length === 0) {
            this.populateNewGrid();
            return;
        }

        for (let row=0; row<this.rows; row++) {
            this.grid[row] = new Array(this.cols);
            for (let col=0; col<this.cols; col++) {
                this.factory("", this.createId(row, col), store.grid.grid[row][col]);
            }
        }
    }

    populateNewGrid() {
        const rows = this.rows;
        const cols = this.cols;

        this.grid = new Array(rows);
        for (let i=0; i<rows; i++) {
            this.grid[i] = new Array(cols);
            for (let j=0; j<cols; j++) {
                const dune = this.prepareDune(i,j);
                this.factory("dune", this.createId(i,j), dune);
            }
        }
    }

    prepareCopyOfStore(name) {
        const ref = deepCopy(store.buildings[name]);
        delete ref.cost;
        delete ref.visible;
        return ref;
    }

    prepareDune(row, col) {
        const dune = deepCopy(store.buildings.dune);
        dune.name = "dune";
        const rc = (this.rows-1) / 2;
        const cc = (this.cols-1) / 2;
        const dist = Math.floor(Math.abs(row-rc)) + Math.floor(Math.abs(col-cc));
        dune.maxLife = Math.pow(4, dist) * 30;
        dune.life = dune.maxLife;
        dune.id = this.createId(row, col);
        return dune;
    }

    save() {
        store.grid.rows = this.rows;
        store.grid.cols = this.cols;
        store.grid.grid = this.grid.map(list => list.map(building => building?.save()));
    }

    tryToBuild(id) {
        const building = this.getBuilding(id);
        
        if (null === building && this.buildingMenuSelected !== "dune" || (building?.name === "dune" && building?.modif === 0 && this.buildingMenuSelected === "dune")) {
            const check = resourceManager.checkCost(this.buildingMenuSelected);

            if (!check) {
                messageManager.msg(MSG.NOT_ENOUGH_RES);
                this.menuBuildingsClick(this.buildingMenuSelected);
                return;
            }

            resourceManager.payCost(this.buildingMenuSelected);

            if ("dune" === this.buildingMenuSelected) {
                building.modif = -1;
                this.actualiseGridEffectIcons(...this.getRowColFromId(id));
            } else {
                this.build(Number(id), this.buildingMenuSelected);
            }
        } else if ("dune" === building?.name) {
            this.menuBuildingsClick(this.buildingMenuSelected);
            messageManager.msg(MSG.CANT_CONSTRUCT_DUNE);
        }
    }

    unlock(name) {
        store.buildings[name].visible = true;
        const div = refManager.get("dom", "building-menu-elem-"+name);
        div.classList.remove("invisible");
    }

    updateBuildings(name, attribute, multiplicande=1) {
        for (let i=0; i<this.rows; i++) {
            for (let j=0; j<this.cols; j++) {
                const building = this.grid[i][j];
                if (name === building?.name) {
                    building[attribute] = multiplicande * store.buildings[name][attribute];
                }
            }
        }
    }

    core() {
        this.advanceTime("electricity");
        this.advanceTime();
        this.advanceTime("laboratories");

        this.display();
    }
}