class GameManager {
    constructor() {
        this.callbacks = [];
    }

    initialize() {
        if (!!!saveManager.load()) {
            this.firstStart();
        }
        resourceManager.init();
        buildingManager.init();
        upgradeManager.init();

        if (!!!store.tuto.seen) {
            tutoManager.start();
        }
    }

    firstStart() {
        resetStore();
    }

    waitForEndOfWork(callback) {
        this.callbacks.push(callback);
    }

    core() {
        buildingManager.core();
        resourceManager.core();
        saveManager.core();

        this.callbacks.forEach(callback => callback());
        this.callbacks = [];
    }
}