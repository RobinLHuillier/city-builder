class ResourceManager {
    constructor() {
        this.res = {};
    }

    init() {
        const resources = refManager.get("dom", "resources");
        resources.innerHTML = "";
        for (const resName in store.resources) {
            const resDiv = document.createElement('div');
            resDiv.id = 'res-div-'+resName;
            resDiv.className = "resourceDiv invisible";
            resDiv.innerHTML = '<span class="resourceName">'+resName.charAt(0).toUpperCase()+resName.slice(1)+' </span><span id="res-span-'+resName+'">0</span><span id="res-max-'+resName+'" class="invisible"></span> (<span id="res-avg-'+resName+'">/</span>)';
            resources.appendChild(resDiv);

            if (resName === "electricity") {
                this.res[resName] = new Electricity(resName, store.resources[resName]);
            } else {
                this.res[resName] = new Resource(resName, store.resources[resName]);
            }
            this.res[resName].updateVisibility();
        };
    }

    add(resName, quantity) {
        this.get(resName)?.add(quantity);
        this.get(resName)?.makeVisible();
    }

    changeStockResource(resName, quantity) {
        const resource = this.get(resName);
        if (null === resource)
            return;
        resource.maxStock += quantity;
    }

    checkCost(buildingName) {
        const building = store.buildings[buildingName];
        if (undefined === building) {
            return false;
        }
        for (const res in building.cost) {
            if (this.getQuantity(res) < building.cost[res]) {
                return false;
            }
        }
        return true;
    }

    checkUsage(usage) {
        let pc = 1;
        for (const res in usage) {
            const q = this.getQuantity(res);
            if (q < usage[res]) {
                const percent = q / usage[res];
                if (percent < pc) {
                    pc = percent;
                }
            }
        }
        return pc;
    }

    display() {
        this.do((resName) => {
            this.res[resName].updateAverage();
            this.res[resName].display();
        })
    }

    do(fn) {
        Object.keys(this.res).forEach(fn);
    }

    get(resName) {
        if (undefined === this.res[resName]) {
            console.error("Resource not in dict", resName);
            return null;
        }
        return this.res[resName];
    }

    getQuantity(resName) {
        return this.get(resName)?.getQuantity();
    }

    payCost(buildingName) {
        const building = store.buildings[buildingName];
        if (undefined === building) {
            return false;
        }
        for (const res in building.cost) {
            this.subtract(res, building.cost[res]);
        }
    }

    save() {
        this.do((resName) => {
            store.resources[resName] = this.res[resName].save();
        });
    }

    subtract(resName, quantity) {
        const subtracted = this.get(resName)?.subtract(quantity);
        if (null === subtracted) {
            return 0;
        }
        return subtracted/quantity;
    }

    tickProd(production, usage) {
        // % of production gettable
        const pc = this.checkUsage(usage);
        // console.log(pc, usage, this.getQuantity("electricity"));
        for (const res in usage) {
            this.subtract(res, pc*usage[res]);
        }
        for (const res in production) {
            this.add(res, pc*production[res]);
        }
        return pc;
    }

    core() {
        this.display();
    }
}