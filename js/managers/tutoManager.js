class TutoManager {
    constructor() {

    }

    start() {
        const div = refManager.get("dom", "tuto");
        div.classList.remove("invisible");
        const tuto = refManager.get("dom", "tuto-text");

        tuto.innerHTML = "You've been sent to mine and exploit the resources of a distant planet. <br> Use robots to assist in your task. <br> But beware ! The intense gravity of the planet will eventually destroy everything you build. <br><br><br> Select building on the left and click on the grid to place them. <br> Use a laboratory to extract research from resources and upgrade your facilities on the right! ";

        const button = refManager.get("dom", "tuto-button");
        button.innerHTML = "UNDERSTOOD";
        button.addEventListener("click", () => tutoManager.nextClick());
    }

    nextClick() {
        const div = refManager.get("dom", "tuto");
        div.classList.add("invisible");
        store.tuto.seen = true;
    }
}