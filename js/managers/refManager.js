class RefManager {
    refs = {};

    constructor() {
        this.refs = {};
    }

    get(type, elem) {
        if (undefined !== this.refs[elem]) {
            return this.refs[elem];
        }

        if ("dom" === type) {
            const ref = document.getElementById(elem);
            if (null === ref) {
                console.error("Cannot get ref for", type, elem);
                return null;
            }
            this.refs[elem] = ref;
            return ref;
        }
        
        console.error("Cannot recognize type ref", type, elem);
        return null;
    }
}