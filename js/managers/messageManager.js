class MessageManager {
    constructor() {
        this.popupRefs = new Array(NUMBER_MAX_OF_POPUPS);
        
        this.init(NUMBER_MAX_OF_POPUPS);
    }

    init(num) {
        const container = refManager.get("dom", "popups");
        for(let i=0; i<num; i++) {
            const popup = document.createElement("div");
            popup.className = "popup invisible-popup";
            popup.id = "popup-"+i.toString();
            popup.innerHTML = "<div class='msg-popup' id='msg-popup-"+i.toString()+"'></div>";
            popup.addEventListener("click", () => this.makeInvisible(i));
            container.appendChild(popup);
            this.popupRefs[i] = false;
        }
    }

    getPopup() {
        for(let i=0; i<NUMBER_MAX_OF_POPUPS; i++) {
            if (!this.popupRefs[i]) {
                this.popupRefs[i] = true;
                return i;
            }
        }
        return null;
    }

    makeInvisible(num) {
        const popup = refManager.get("dom", "popup-"+num.toString());
        if (popup.classList.contains("invisible-popup")) {
            return;
        }
        popup.classList.add("invisible-popup");

        const popupMsg = refManager.get("dom", "msg-popup-"+num.toString());
        setTimeout(() => {
            popup.classList.add("squish-popup");
            this.popupRefs[num] = false;
        }, 1100);
    }

    msg(content, timer=null) {
        const popupNum = this.getPopup();
        if (null === popupNum) {
            return;
        }

        const popup = refManager.get("dom", "popup-"+popupNum.toString());
        popup.classList.remove("invisible-popup");
        popup.classList.remove("squish-popup");

        const popupMsg = refManager.get("dom", "msg-popup-"+popupNum.toString());
        popupMsg.innerHTML = content;

        setTimeout(() => {
            this.makeInvisible(popupNum);
        }, null === timer ? POPUP_TIMER : timer);
    }
}