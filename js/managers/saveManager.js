class SaveManager {
    constructor() {
        this.timestamp = lastTime;
    }

    advanceTime() {
        if (lastTime < this.timestamp + saveInterval) {
            return false;
        }
        this.timestamp = lastTime;
        messageManager.msg(MSG.SAVING, POPUP_SAVE_TIMER);
        return true;
    }

    load() {
        if (DEBUG_NO_SAVE)
            return false;
        
        const retrievedSave = localStorage.getItem(LOCAL_STORAGE_KEY);
        if (null === retrievedSave)
            return false;
        store = JSON.parse(retrievedSave);
        return true;
    }

    reset() {
        gameManager.waitForEndOfWork(() => {
            initHandlers();
            resetStore();
            this.save(true);
            gameManager.initialize();
        });
    }

    save(onlyStore=false) {
        if (!!!onlyStore) {
            resourceManager.save();
            upgradeManager.save();
            buildingManager.save();
        }

        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(store));
    }

    core() {
        if (this.advanceTime()) {
            this.save();
        }
    }
}