class UpgradeManager {
    constructor() {
        this.upgrades = {};
        this.upgradeSelected = null;
    }

    init() {
        const menuDiv = refManager.get("dom", "upgrades");
        menuDiv.innerHTML = "<div class='title-menu'>Research</div>";
        for (const upName in store.upgrades) {
            const div = document.createElement("div");
            div.id = "upgrade-"+upName;
            div.className = "upgrade-menu-elem tooltip tooltip-left";
            div.innerHTML = "<div class='progress-upgrade'><div id='slider-upgrade-" + upName + "'></div></div><img src=assets/img/menu-build/"+store.upgrades[upName].buildingTarget+".png>" + this.buildTooltip(store.upgrades[upName], upName); 
            div.addEventListener("click", () => this.upgradeClick(upName));
            menuDiv.appendChild(div);

            const upgrade = this.factory(upName);
            this.upgrades[upName] = upgrade;
            upgrade.updateVisibility();
            this.updateTooltip(upgrade);
            this.updateProgress(upgrade);
        }
    }

    applyUpgrade(upgrade) {
        if (undefined !== upgrade.effect["unlock-building"]) {
            for (const building of upgrade.effect["unlock-building"]) {
                buildingManager.unlock(building);
            }
        }

        if (undefined !== upgrade.effect["unlock-upgrade"]) {
            for (const name of upgrade.effect["unlock-upgrade"]) {
                this.upgrades[name].visible = true;
                this.upgrades[name].updateVisibility();
                this.updateTooltip(this.upgrades[name]);
            }
        }

        if (undefined !== upgrade.effect["upgrade-building"]) {
            const target = upgrade.effect["upgrade-building"].name;
            if (undefined === store.buildings[target]) {
                console.error("building undefined", target);
                return;
            }

            const production = upgrade.effect["upgrade-building"].production;
            const durability = upgrade.effect["upgrade-building"].durability;
            const xNeighbors = upgrade.effect["upgrade-building"].xNeighbors;

            if (undefined !== production) {
                Object.keys(store.buildings[target].produce).forEach(resName => store.buildings[target].produce[resName] *= production);
            }

            if (undefined !== durability) {
                store.buildings[target].maxLife *= durability;
            }

            if (undefined !== xNeighbors) {
                Object.keys(store.buildings[target].effect).forEach(effectName => {
                    if (undefined !== store.buildings[target].effect[effectName].xNeighbors)
                        store.buildings[target].effect[effectName].xNeighbors = xNeighbors;
                })
            }


            // TODO : big opti possible, we reconstruct the whole menu juste to update an info on a tooltip
            buildingManager.constructBuildMenu();
        }

        if (undefined !== upgrade.effect["des-speed"]) {
            const speed = upgrade.effect["des-speed"];
            store.buildings.dune.desSpeed *= speed;
            const mult = 1/1000; // to ms
            buildingManager.updateBuildings("dune", "desSpeed", mult);
        }
    }

    buildTooltip(upgrade, refName) {
        // ex : tt-upgrade-showMine-name
        const ident = (ct) => "id='tt-upgrade-" + refName + "-" + ct + "'";

        const separator = "<div class='tooltip-content-separator'>--- ---</div>";
        
        let tooltip = "<div class='tooltip-content'>";

        tooltip += "<div class='tooltip-content-name' " + ident("name") + ">" + upgrade.name + "</div>" + separator;

        tooltip += "<div class='tooltip-content-description' " + ident("description") + ">" + upgrade.description + "</div>" + separator;

        tooltip += "<div class='tooltip-content-cost'>Cost: <span " + ident("cost") + ">?<span></div>";

        tooltip += separator;

        tooltip += "<div class='tooltip-content-research'>Remaining time: <span " + ident("research-time") + ">?</span></div>";

        tooltip += "</div>";
        return tooltip;
    }

    factory(upName) {
        const upgrade = deepCopy(store.upgrades[upName]);

        if (undefined === upgrade.cost) {
            upgrade.cost = deepCopy(upgrade.maxCost);
        }

        return new Upgrade(upName, upgrade);
    }

    finishUpgrade(upgrade) {
        upgrade.bought = true;
        this.upgradeSelected = null;
        upgrade.updateVisibility();

        messageManager.msg("The research '" + upgrade.name + "' has been finished.");

        this.applyUpgrade(upgrade);
    }

    researchTick(quantity) {
        if (null === this.upgradeSelected || quantity == 0) {
            return;
        }

        const upgrade = this.upgrades[this.upgradeSelected]
        let q = quantity;
        for (const res in upgrade.cost) {
            if (q <= 0) {
                break;
            }
            if (upgrade.cost[res] <= 0) {
                continue;
            }
            const subtracted = resourceManager.subtract(res, q);
            upgrade.cost[res] -= subtracted*q;
            q -= subtracted*q;
        }

        upgrade.updateCostTotal();
        this.updateTooltip(upgrade);
        this.updateProgress(upgrade);

        if (Object.values(upgrade.cost).filter(val => val > 0).length === 0) {
            this.finishUpgrade(upgrade);
        }
    }

    save() {
        for (const upName in this.upgrades) {
            store.upgrades[upName] = this.upgrades[upName].save();
        }
    }

    updateProgress(upgrade) {
        if (!!!upgrade.visible || upgrade.bought) {
            return;
        }

        const pc = upgrade.getRatioProgress();
        const height = Math.floor(100*pc);
        const progressDiv = refManager.get("dom", "slider-upgrade-"+upgrade.refName);
        progressDiv.style.height = height.toString() + "%";
    }

    updateTooltip(upgrade) {
        if (!!!upgrade.visible || upgrade.bought) {
            return;
        }
        
        const costSpan = refManager.get("dom", "tt-upgrade-"+upgrade.refName+"-cost");
        costSpan.innerHTML = Object.entries(upgrade.cost).map(([resName, resCost]) => `${resName} ${formatNumber(resCost)}`).join(" | ");

        const researchTimeSpan = refManager.get("dom", "tt-upgrade-"+upgrade.refName+"-research-time");
        const labConsumption = buildingManager.getMaxLabConsumption();
        const costRemaining = Object.values(upgrade.cost).reduce((acc, val) => acc + val, 0);
        researchTimeSpan.innerHTML = secToTime(costRemaining/labConsumption);
    }

    upgradeClick(name) {
        const elem = refManager.get("dom", "upgrade-"+name);
        if (name === this.upgradeSelected) {
            elem.classList.remove("menu-selected");
            this.upgradeSelected = null;
        } else {
            elem.classList.add("menu-selected");

            if (null !== this.upgradeSelected) {
                const elemToReset = refManager.get("dom", "upgrade-"+this.upgradeSelected);
                elemToReset.classList.remove("menu-selected");
            }
            this.upgradeSelected = name;
        }
    }

    core() {

    }
}