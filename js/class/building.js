class Building {
    constructor(
        name,
        gridID,
        json,
    ) {
        this.name = name;
        this.id = gridID;
        this.maxLife = json.maxLife;
        this.produce = json.produce;
        this.endProduce = json.endProduce;
        this.usage = json.usage;
        // adapt to ms
        this.consSpeed = DEBUG_CONS_SPEED * json.consSpeed/1000;
        this.desSpeed = json.desSpeed/1000;
        this.imageNumber = json.imageNumber;

        this.description = json.description;
        this.effect = json.effect;
        this.realName = json.realName;

        this.imgRef = refManager.get("dom", "image-"+gridID);
        this.sliderRef = refManager.get("dom", "slider-"+gridID);

        this.life = undefined === json.life ? 0 : json.life;
        this.modif = undefined === json.modif ? 1 : json.modif;
    }

    advance(time) {
        // time in ms, return a triple (production, usage, deletion) with all production and usage value and if it must be deleted
        const production = {};
        const usage = {};
        let deletion = false;
        let construction = false;

        if (this.modif === 1) {
            const remainTime = (this.maxLife - this.life)/this.consSpeed;

            if (time <= remainTime) {
                this.life += (time * this.consSpeed);
            } else {
                time -= remainTime;
                this.modif = -1;
                construction = true;
            }
        }

        if (this.modif === -1) {
            const remainTime = this.life/this.desSpeed;

            let productTime = time;
            if (time <= remainTime) {
                this.life -= (time * this.desSpeed);
            } else {
                this.life = 0;
                deletion = true;
                productTime = remainTime;
            }
            
            for (const res in this.produce) {
                production[res] = this.produce[res]*productTime/1000;
            }
            for (const res in this.usage) {
                usage[res] = this.usage[res]*productTime/1000;
            }
        }

        this.display();
        return [production, usage, deletion, construction];
    }

    display() {
        const imageAppendice = this.modif === 1 ? "cons" : "des";
        const src = "assets/img/buildings/" + this.name + "-" + imageAppendice + "-" + this.getCurrentImageNumber().toString() + ".png";
        if (!!!this.imgRef.src.endsWith(src)) {
            this.imgRef.src = src;
        }

        this.sliderRef.style.backgroundColor = this.modif === 1 ? COLOR_CONS : COLOR_DES;
        this.sliderRef.style.width = Math.floor(100*this.life/this.maxLife).toString() + "%";
    }

    getCurrentImageNumber() {
        // 0.999 to force rounding error and never obtain 1
        return 1+Math.floor(this.imageNumber*this.life*0.999/this.maxLife);
    }

    save() {
        const json = deepCopy(this);
        json.desSpeed *= 1000;
        json.consSpeed *= 1000;
        delete json.imgRef;
        delete json.sliderRef;
        return json;
    }
}