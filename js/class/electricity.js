class Electricity extends Resource {
    constructor(
        name,
        json,
    ) {
        super(name, json);

        this.virtualStock = 0;
        this.timestamp = null;
        this.stock = json.quantity;
        this.maxStock = json.maxStock !== undefined ? json.maxStock : 0;

        this.refMax = refManager.get("dom", "res-max-"+name);
    }

    add(quantity) {
        if (null === this.timestamp) {
            this.timestamp = lastTime;
        } else if (this.timestamp + refresh < lastTime) {
            this.timestamp = lastTime;
            this.transferVirtualStock();
        }
        this.virtualStock += quantity;
    }

    getQuantity() {
        return this.virtualStock + this.stock;
    }

    subtract(quantity) {
        let q = 0;
        if (quantity > this.virtualStock) {
            q += this.virtualStock;
            this.virtualStock = 0;
            this.timestamp = null;
        } else {
            this.virtualStock -= quantity;
            return quantity;
        }
        if (quantity - q > this.stock) {
            q += this.stock;
            this.stock = 0;
            return q;
        }
        this.stock -= (quantity - q);
        return quantity;
    }

    transferVirtualStock() {
        this.stock += this.virtualStock;
        this.stock = Math.min(this.stock, this.maxStock);
        this.virtualStock = 0;
    }

    updateVisibility() {
        super.updateVisibility();

        this.refMax.classList.remove("invisible");
    }

    display() {
        super.display();
        this.refMax.textContent = " / " + formatNumber(this.maxStock) + " ";
    }
}