class Upgrade {
    constructor(
        refName,
        json,
    ) {
        this.refName = refName;
        this.name = json.name;
        this.description = json.description;
        this.buildingTarget = json.buildingTarget;
        this.cost = json.cost;
        this.maxCost = json.maxCost;
        this.effect = json.effect;
        this.visible = json.visible;
        this.bought = json.bought;
        // precalculated
        this.totalCost = Object.values(this.maxCost).reduce((acc, val) => acc+val, 0);
        this.currentCost = 0;
        this.updateCostTotal();
        this.divRef = refManager.get("dom", "upgrade-"+this.refName);
    }

    getRatioProgress() {
        return 1 - (this.currentCost / this.totalCost);
    }

    save() {
        const json = deepCopy(this);
        delete json.refName;
        delete json.divRef;
        delete json.totalCost;
        delete json.currentCost;
        return json;
    }

    updateCostTotal() {
        this.currentCost = Object.values(this.cost).reduce((acc, val) => acc+val, 0);
    }

    updateVisibility() {
        if (this.bought || !!!this.visible) {
            this.divRef.classList.add("invisible");
        } else {
            this.divRef.classList.remove("invisible");
        }
    }
}