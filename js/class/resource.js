class Resource {
    constructor(
        name,
        json,
    ) {
        this.name = name;
        this.quantity = json.quantity;
        this.visible = json.visible;

        this.avgWindow = [{timestamp: lastTime, quantity: this.quantity}];
        this.refSpan = refManager.get("dom", "res-span-"+name);
        this.refAvg = refManager.get("dom", "res-avg-"+name);
        this.refDiv = refManager.get("dom", "res-div-"+name);
    }

    add(quantity) {
        this.quantity += quantity;
    }

    subtract(quantity) {
        if (quantity > this.quantity) {
            const q = this.quantity;
            this.quantity = 0;
            return q;
        }
        this.quantity -= quantity;
        return quantity;
    }

    getQuantity() {
        return this.quantity;
    }

    makeVisible() {
        if (this.visible)
            return;
        this.visible = true;
        this.updateVisibility();
    }

    save() {
        return {
            quantity: this.quantity,
            visible: this.visible,
        };
    }

    updateAverage() {
        const last = this.avgWindow.at(-1);
        if (last.timestamp + 1000 <= lastTime) {
            this.avgWindow.push({timestamp: lastTime, quantity: this.getQuantity()});
        }
        this.avgWindow = this.avgWindow.slice(-11); // only keep the last 10 secs + 1 sec buffer
    }

    updateVisibility() {
        if (!this.visible) {
            this.refDiv.classList.remove('visible');
            this.refDiv.classList.add('invisible');
        } else {
            this.refDiv.classList.remove('invisible');
            this.refDiv.classList.add('visible');
        }
    }

    display() {
        this.refSpan.textContent = formatNumber(this.getQuantity());
        const avg = this.avgWindow.length < 3 ? 0 : (this.avgWindow.at(-2).quantity - this.avgWindow.at(0).quantity) / (this.avgWindow.length-2);
        this.refAvg.innerHTML = formatNumber(avg, 2) + "/s"
    }
}