var buildingManager;
var gameManager;
var messageManager;
var refManager;
var resourceManager;
var saveManager;
var tutoManager;
var upgradeManager;

function initHandlers() {
    // used for others so first
    refManager = new RefManager();

    buildingManager = new BuildingManager();
    messageManager = new MessageManager();
    resourceManager = new ResourceManager();
    saveManager = new SaveManager();
    tutoManager = new TutoManager();
    upgradeManager = new UpgradeManager();
}

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        deltaTime = timestamp - lastTime;
        // console.time("core");
        gameManager.core();
        // console.timeEnd("core");
        timer += deltaTime;
        lastTime = timestamp;
    }
    window.requestAnimationFrame(animate);
}

window.onload = function() {
    initHandlers();
    gameManager = new GameManager();
    gameManager.initialize();

    launchUnitTests();

    animate(performance.now());
}