const DEBUG_CONS_SPEED = 1;
const DEBUG_NO_SAVE = false;

const SYMBOL_INFINITY = "∞";

// save
const LOCAL_STORAGE_KEY = "city-builder-tete-de-steak";
const saveInterval = 10000; // ms

// unit-tests
const UNIT_TESTS = true;

//time
const refresh = 49.5; // a tick requested is 16.66 ms
const tickPerSec = 1000/refresh;
var deltaTime = 1;  // ms
var lastTime = 0;
var timer;

//colors
const COLOR_CONS = "blue";
const COLOR_DES = "red";

// popups
const NUMBER_MAX_OF_POPUPS = 20;
const POPUP_TIMER = 20000; // ms
const POPUP_SAVE_TIMER = 1000;

// effects
const EFFECT_STOCK_ELECTRICITY = "stock-elec";
const EFFECT_PROTECTION = "protection";
const EFFECT_CONSTRUCTION = "construction";
const EFFECT_PRODUCTION = "production";
const EFFECT_CONSUMPTION = "consumption";
const EFFECT_AUTOMATISATION_CONSTRUCTION = "automatisation";

// text
const MSG = {
    NOT_ENOUGH_RES: "Not enough resources to build :(",
    CANT_CONSTRUCT_DUNE: "Can't construct, please clean the rubbles before !",
    SAVING: "Saving ..."
};