function rand(max, min) {
    if (undefined === min) {
        min = 0;
    }
    return Math.floor(Math.random() * (max-min)) + min;
}

function randElem(array) {
    return array[rand(array.length)];
}

function delElem(array, elem) {
    if (array.indexOf(elem) === -1) {
        console.error("[delElem] trying to delete an element non existent", array, elem);
        return;
    }
    array.splice(array.indexOf(elem), 1);
}

function formatNumber(num, decimals=0) {
    if (num < 10000) {
        if (decimals === 0) {
            return Math.floor(num).toString();
        }
        const pow = Math.pow(10, decimals);
        return (Math.floor(num*pow)/pow).toString();
    }
    let power = 4;
    num /= 10000;
    while (num >= 10) {
        power++;
        num /= 10;
    }
    return (Math.floor(100*num)/100).toString() + "e" + power.toString();
}

function shuffle(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
}

function secToTime(sec) {
    if (Infinity === sec) {
        return SYMBOL_INFINITY;
    }
    const min = sec/60;
    const hours = min/60;
    const days = hours/24;
    if (min < 1) {
        return (Math.floor(sec*10)/10).toString()+"s";
    }
    if (hours < 1) {
        return (Math.floor(min*100)/100).toString()+"m";
    }
    if (days < 1) {
        return (Math.floor(hours*100)/100).toString()+"h";
    }
    return (Math.floor(days*100)/100).toString()+"d";
}

function ticksToTime(tick) {
    const sec = tick/tickPerSec;
    return secToTime(sec);
}

function deepCopy(object) {
    return JSON.parse(JSON.stringify(object));
}