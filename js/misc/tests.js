const unitTests = {
    electricity: () => {
        const elec = new Electricity("electricity", {quantity: 10, visible: true, maxStock: 10});

        lastTime = 4;
        elec.add(4);
        if (elec.getQuantity() !== 14)
            return false;

        lastTime += 2*refresh;
        elec.add(2);
        if (elec.getQuantity() !== 12)
            return false;

        if (elec.subtract(4) !== 4)
            return false;
        if (elec.virtualStock !== 0)
            return false;
        if (elec.timestamp !== null)
            return false;
        if (elec.stock !== 8)
            return false;

        elec.add(15);
        if (elec.subtract(5454) !== 23)
            return false;

        return true;
    },
    gridNeighbors: () => {
        const bm = new BuildingManager();
        bm.rows = 10;
        bm.cols = 10;

        if (bm.getAllCoorNeighbours(4,4,4).length !== 4) 
            return false;
        if (bm.getAllCoorNeighbours(4,4,8).length !== 8) 
            return false;
        if (bm.getAllCoorNeighbours(4,4,12).length !== 12) 
            return false;

        if (bm.getAllCoorNeighbours(0,0,4).length !== 2) 
            return false;

        if (bm.getAllCoorNeighbours(9,9,8).length !== 3) 
            return false;

        return true;
    },

    reset: () => {
        lastTime = 0; 

        return true;
    }
};

function launchUnitTests () {
    if (UNIT_TESTS) {
        let testsFailed = 0;
        for (const name in unitTests) {
            unitTests.reset();
            if (!!!unitTests[name]()) {
                console.log("UNIT TEST FAILED : ", name);
                testsFailed++;
            }
        }
        if (testsFailed > 0) {
            console.error(testsFailed, "tests failed");
        } else {
            console.log("All tests passed :)");
        }
    }
}